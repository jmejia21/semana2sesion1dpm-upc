package pe.edu.upc.androidapp1.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Alumnos on 25/10/2017.
 */

public class Activity1 extends Activity implements View.OnClickListener{

    EditText txvUsuario;
    EditText txvPassword;
    Button btnIngresar;

    Button btnRecuperar;

    TextView txtDatosIncompletos;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity1);

        txvUsuario = (EditText) this.findViewById(R.id.editTex1tAct1);
        txvPassword = (EditText) this.findViewById(R.id.editTex2tAct1);

        txtDatosIncompletos = (TextView) this.findViewById(R.id.txtDatosIncompletos);

        btnIngresar = (Button) this.findViewById(R.id.btn1Act);
        btnRecuperar = (Button) this.findViewById(R.id.btnRecuperacion);

        btnIngresar.setOnClickListener(this);
        btnRecuperar.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        //Toast.makeText(this,"on clic",Toast.LENGTH_SHORT).show();

        switch (v.getId()) {

            case R.id.btn1Act:


                final String usuario = txvUsuario.getText().toString();
                final String pass = txvPassword.getText().toString();
                if (usuario.equals("") || pass.equals("")) {
                    txtDatosIncompletos.setText("* Datos incompletos");
                }else{
                    txtDatosIncompletos.setText("");
                    Intent intent2  = new Intent(this,Activity2.class);
                    intent2.putExtra("usuario",txvUsuario.getText().toString());
                    this.startActivity(intent2);
                }

                break;

            case R.id.btnRecuperacion:

                Intent intent3  = new Intent(this,Activity3.class);
                this.startActivity(intent3);
                break;

            default:
                break;
        }





    }




}
