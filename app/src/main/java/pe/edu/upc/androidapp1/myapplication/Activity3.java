package pe.edu.upc.androidapp1.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by JoseLuis on 29/10/17.
 */

public class Activity3 extends Activity {


    TextView lblTitulo;
    TextView lblCorreoElectronico;
    EditText txtCorreoElectronico;
    Button btnEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity3);
        lblTitulo = (TextView) this.findViewById(R.id.lblTitulo);
        lblCorreoElectronico = (TextView) this.findViewById(R.id.lblCorreoElectronico);
        txtCorreoElectronico = (EditText) this.findViewById(R.id.txtCorreoElectronico);
        btnEnviar = (Button) this.findViewById(R.id.btnEnviar);

    }
}
