package pe.edu.upc.androidapp1.myapplication;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Alumnos on 25/10/2017.
 */

public class Activity2 extends Activity {

    TextView txvBienvenida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);


        Intent intent = getIntent();
        String message = intent.getStringExtra("usuario");


        txvBienvenida = (TextView) this.findViewById(R.id.txtView2Act2);
        txvBienvenida.setText(message);
    }
}
